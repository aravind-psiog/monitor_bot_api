from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def hash_password(password):
    hashed_password = pwd_context.hash(password)
    return hashed_password


def verify_hash(plain,hashed):
    return pwd_context.verify(plain,hashed)